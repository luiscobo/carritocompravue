export default {
    data:{
        productosCarrito: [],
        total:0,
    },

    agregarProducto(producto){
        // Comprobamos si ya existe el producto en el carrito.
        let productoActual = this.data.productosCarrito.find(p=>p.idProducto==producto.id);
        
        if(productoActual==null){
        let productoNuevo = {
            idProducto: producto.id,
            nombreProducto: producto.nombre,
            imgProducto: producto.imagen,
            cantidadProducto: 1,
            precioProducto: producto.precio,
            totalProducto: producto.precio
        }  
        this.data.productosCarrito.push(productoNuevo);  
            
        } else {
        productoActual.cantidadProducto++;
        productoActual.totalProducto=productoActual.precioProducto*productoActual.cantidadProducto;
        }   
    // Actualizamos el total
    this.data.total = this.data.total+producto.precio
    },

    eliminarProductoCarrito(producto){
        let productoEliminar = this.data.productosCarrito.find(p=>p.idProducto==producto.idProducto);
    
        if(productoEliminar.cantidadProducto==1){
          // Buscamos el indice del elemento
          let index = this.data.productosCarrito.findIndex(p => p == producto.idProducto);
          // Eliminamos el elemento
          this.data.productosCarrito.splice(index,1);     
        } else{
          productoEliminar.cantidadProducto=productoEliminar.cantidadProducto-1;
        }
        // Actualizamos el total
        this.data.total = this.data.total-producto.precioProducto       
      }

}