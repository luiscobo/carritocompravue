import base64 from '../util/base64'

export default {

    data:{
      categorias : [
        {nombre: 'Moviles'},
        {nombre: 'Ordenadores'}
      ],  
      // Hacemos la carga inicial de los productos
      productos :  [
        {
            id: 1,
            nombre: 'Iphone 11',
            descripcion: 'Blanco, 64 GB, 6.1" Liquid Retina HD, Chip A13 Bionic, iOS',
            categoria: 'Moviles',
            precio: 659,
            stock: 5,
            imagen: 'data:image/jpeg;base64,' + base64.getImagen('Iphone 11')
        },
        {
            id: 2,
            nombre: 'Samsung S20',
            descripcion: '4G, Blanco, 128GB, 6GB RAM, 6.5" Full HD+, Exynos 990, 4500 mAh, IP68, Android',
            categoria: 'Moviles',
            precio: 599,
            stock: 10,
            imagen: 'data:image/jpeg;base64,' + base64.getImagen('Samsung S20')
        },
        {
            id: 3,
            nombre: 'Huawei P30',
            descripcion: 'Azul, 128 GB, 8 GB, 6.1" Full HD+, Kirin 990, 3800 mAh, 5G, Android',
            categoria: 'Moviles',
            precio: 519,
            stock: 15,
            imagen: 'data:image/jpeg;base64,' + base64.getImagen('Huawei P30')
        },
        {
            id: 4,
            nombre: 'Iphone X',
            descripcion: 'Oro, 128 GB, 3 GB RAM, 6.1" Liquid Retina HD, Chip A12 Bionic, iOS',
            categoria: 'Moviles',
            precio: 633,
            stock: 20,
            imagen: 'data:image/jpeg;base64,' + base64.getImagen('Iphone X')
        },
        {
            id: 5,
            nombre: 'Macbook Air',
            descripcion: '13.3" Retina, Apple Silicon M1, 8 GB, 256 GB SSD, MacOS, Gris espacial',
            categoria: 'Ordenadores',
            precio: 1129,
            stock: 30,
            imagen: 'data:image/jpeg;base64,' + base64.getImagen('Macbook Air')
        },
        {
            id: 6,
            nombre: 'Macbook Pro',
            descripcion: 'Retina, Apple Silicon M1, 8 GB, 256 GB SSD, MacOS, Gris espacial',
            categoria: 'Ordenadores',
            precio: 1179,
            stock: 10,
            imagen: 'data:image/jpeg;base64,' + base64.getImagen('Macbook Pro')
        },
        {
            id: 7,
            nombre: 'Surface',
            descripcion: 'Convertible 2 en 1, 12.3", Intel® Core™ i5-1035G4, 8GB RAM, 128GB, W10',
            categoria: 'Ordenadores',
            precio: 1049,
            stock: 10,
            imagen: 'data:image/jpeg;base64,' + base64.getImagen('Surface')
        },
        {
            id: 8,
            nombre: 'Mac',
            descripcion: '21.5" HD, Intel® Core™ i5-7360U, Iris® Plus Graphics 640',
            categoria: 'Ordenadores',
            precio: 1300,
            stock: 10,
            imagen: 'data:image/jpeg;base64,' + base64.getImagen('Mac')
        }
      ]
    },

    inicializarProductos() {

        //Recuperamos o almacenamos los datos de localStorage
        if(JSON.parse(localStorage.getItem('categoriasVue'))==null){
            localStorage.setItem('categoriasVue',JSON.stringify(this.data.categorias));
                      
        } else {
            this.data.categorias = JSON.parse(localStorage.getItem('categoriasVue'));     
                 
        }
    
        if(JSON.parse(localStorage.getItem('productosVue'))==null){
            localStorage.setItem('productosVue',JSON.stringify(this.data.productos));
        } else {
            this.data.productos = JSON.parse(localStorage.getItem('productosVue'));
        }
    },

    //Función para obtener un producto por su id
    getProductoById(id) {
        return this.data.productos.find(p=>p.id==id);
    },

    //Función para obtener todos los productos
    getListaProductos() {
        return this.data.productos
    },

    //Función para obtener todos las categorias
    getListaCategorias() {
        console.log(this.categorias)
        return this.data.categorias
    },

    //Función para obtener todos los productos de una categoría
    getListaProductosByCategoria(cat) {
        return this.data.productos.filter(producto => producto.categoria == cat);
    },

    //Función para actualizar el stock de un producto
    actualizarStock(id, nuevoStock){      
        let producto = this.data.productos.find(p=>p.id==id);
        producto.stock = nuevoStock;
    },

    //Función para guardar un producto nuevo
    guardarProducto(producto){    
        producto.id=this.data.productos.length+1,     
        this.data.productos.push(producto);
        //actualizamos los datos de localStorage
        localStorage.setItem('productosVue',JSON.stringify(this.data.productos));
    },

    //Función para guardar una categoria nueva
    guardarCategoria(categoria){       
        this.data.categorias.push({nombre: categoria});
        //actualizamos los datos de localStorage
        localStorage.setItem('categoriasVue',JSON.stringify(this.data.categorias));
    }
}