import Vue from 'vue'
import VueRouter from 'vue-router'
import CompraComponent from '../components/CompraComponent.vue'
import AdminComponent from '../components/AdminComponent.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'CompraComponent',
    component: CompraComponent
  },
  {
    path: '/admin',
    name: 'AdminComponent',
    component: AdminComponent
  }
]

const router = new VueRouter({
  routes
})

export default router
